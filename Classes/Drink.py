from abc import ABC, abstractmethod


# According to the Decorator pattern this would be the interface for the Component
class Drink(ABC):
    def __init__(self):
        self._order = ''

    @property
    @abstractmethod
    def order(self):
        return self._order

    @order.setter
    def order(self, value):
        self._order = value

    @abstractmethod
    def prepare(self):
        pass
