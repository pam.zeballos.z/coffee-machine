from Classes.Drink import Drink
from Classes.Price import Price


class Chocolate(Drink, Price):
    def __init__(self):
        super().__init__()
        self.price = 0.5
        self._order = 'Chocolate'

    @property
    def order(self):
        return self._order

    def prepare(self):
        pass
