from ..Drink import Drink
from ..Price import Price


class OrangeJuice(Drink, Price):
    def __init__(self):
        super().__init__()
        self.price = 0.6
        self._order = 'Orange Juice'

    @property
    def order(self):
        return self._order

    def prepare(self):
        pass
