from Classes.Drink import Drink
from Classes.Price import Price


class Coffee(Drink, Price):
    def __init__(self):
        super().__init__()
        self.price = 0.6
        self._order = 'Coffee'

    @property
    def order(self):
        return self._order

    def prepare(self):
        pass
