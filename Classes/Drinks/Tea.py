from Classes.Drink import Drink
from Classes.Price import Price


class Tea(Drink, Price):
    def __init__(self):
        super().__init__()
        self.price = 0.4
        self._order = 'Tea'

    @property
    def order(self):
        return self._order

    def prepare(self):
        pass
