from abc import ABC
from .Drink import Drink


class DrinkDecorator(Drink, ABC):

    _drink: Drink = None

    def __init__(self, drink: Drink) -> None:
        super().__init__()
        self._drink = drink
        self._order = drink.order

    @property
    def drink(self) -> Drink:
        """
        The Decorator delegates all work to the wrapped component.
        """
        return self._drink

    @property
    def order(self):
        return self._drink.order


    def prepare(self):
        self._drink.prepare()
