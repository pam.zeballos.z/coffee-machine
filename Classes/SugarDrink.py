from abc import ABC
from .DrinkDecorator import DrinkDecorator


class SugarDrink(DrinkDecorator, ABC):
    def __init__(self, drink, sugar):
        super().__init__(drink)
        self._order = drink.order + '-' + sugar + ' sugar-stick'

    @property
    def order(self):
        return self._order

    def prepare(self):
        pass
