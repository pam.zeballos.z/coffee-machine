from abc import ABC
from .DrinkDecorator import DrinkDecorator


# According to the Decorator pattern this would be the Concrete Component
class Hot(DrinkDecorator, ABC):

    def __init__(self, drink):
        super().__init__(drink)
        self._drink = drink
        self._order = drink.order + '-Extra hot'

    @property
    def order(self):
        return self._order

    def prepare(self):
        pass

