from .Drink import Drink
from .SugarDrink import SugarDrink
from .Hot import Hot
from .Drinks.Tea import Tea
from .Drinks.Coffee import Coffee
from .Drinks.Chocolate import Chocolate
from .Drinks.OrangeJuice import OrangeJuice


class DrinkMaker:

    def process_command(self, command) -> str:
        if command[0] == 'M':
            return self.send_message(command[1])
        else:
            drink = self.make_drink(command[0], command[1], command[2])
            return drink

    def make_drink(self, drink_type, sugar, money):
        drink: Drink

        if drink_type.startswith('T'):
            drink = Tea()
        if drink_type.startswith('H'):
            drink = Chocolate()
        if drink_type.startswith('C'):
            drink = Coffee()
        if drink_type == 'O':
            drink = OrangeJuice()

        result = drink.check_price(money)
        if result == 0:
            if sugar != '':
                drink = SugarDrink(drink, sugar)
            if 'h' in drink_type:
                drink = Hot(drink)

        else:
            return self.send_message(result)

        return drink

    def send_message(self, message) -> str:
        print(message)
        return str(message)

# Decorator pattern
# It would be interesting to first show the solution without the decorator pattern and then with it. Or even include
# The decorator pattern in the presentation. This new implementation would also work with c# as you are not dependant
# of the multiple inheritance for this new approach.


# Presentation decorator + factory + diagrams
# Do another presentation about Angular. Probably going more to the backend side. it takes a time to grasp new knowledge
# Start searching someone else who could fill the NIT role in a year time frame SA
# to be staff I need to learn how to remove road-blocks on my own
