from abc import ABC
from typing import Union


class Price(ABC):
    def __init__(self):
        self.price = 0

    def check_price(self, amount) -> Union[float, int]:
        amount = float(amount)
        price = float(self.price)
        if amount < price:
            return round(price - amount, 2)
        else:
            return 0
