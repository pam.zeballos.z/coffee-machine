import unittest

from Classes.DrinkMaker import DrinkMaker


class DrinkMaterTest(unittest.TestCase):
    def test_tea_with_sugar_and_stick(self):
        command = ['T', '1', '0.4']
        drink_maker = DrinkMaker()
        drink = drink_maker.process_command(command)
        expected = 'Tea-1 sugar-stick'
        self.assertEqual(expected, drink.order)

    def test_chocolate_with_no_sugar_and_no_stick(self):
        command = ['H', '', '0.5']
        drink_maker = DrinkMaker()
        drink = drink_maker.process_command(command)
        expected = 'Chocolate'
        self.assertEqual(expected, drink.order)

    def test_coffee_with_2_sugar_and_stick(self):
        command = ['C', '2', '0.6']
        drink_maker = DrinkMaker()
        drink = drink_maker.process_command(command)
        expected = 'Coffee-2 sugar-stick'
        self.assertEqual(expected, drink.order)

    def test_drink_maker_returns_message(self):
        command = ['M', 'Message']
        drink_maker = DrinkMaker()
        result = drink_maker.process_command(command)
        expected = 'Message'
        self.assertEqual(expected, result)

    def test_tea_with_sugar_and_no_enough_money(self):
        command = ['T', '1', '0.2']
        drink_maker = DrinkMaker()
        result = drink_maker.process_command(command)
        expected = '0.2'
        self.assertEqual(expected, result)

    def test_chocolate_with_no_sugar_and_no_enough_money(self):
        command = ['H', '', '0.1']
        drink_maker = DrinkMaker()
        result = drink_maker.process_command(command)
        expected = '0.4'

        self.assertEqual(expected, result)

    def test_coffee_with_2_sugar_and_no_enough_money(self):
        command = ['C', '2', '0.3']
        drink_maker = DrinkMaker()
        result = drink_maker.process_command(command)
        expected = '0.3'
        self.assertEqual(expected, result)

    def test_orange_juice(self):
        command = ['O', '', '0.6']
        drink_maker = DrinkMaker()
        drink = drink_maker.process_command(command)
        expected = 'Orange Juice'
        self.assertEqual(expected, drink.order)

    def test_orange_juice_and_no_enough_money(self):
        command = ['O', '', '0.2']
        drink_maker = DrinkMaker()
        result = drink_maker.process_command(command)
        expected = '0.4'
        self.assertEqual(expected, result)

    def test_extra_hot_tea_with_sugar_and_stick(self):
        command = ['Th', '2', '0.4']
        drink_maker = DrinkMaker()
        drink = drink_maker.process_command(command)
        expected = 'Tea-2 sugar-stick-Extra hot'
        self.assertEqual(expected, drink.order)

    def test_coffee_with_2_sugar_and_stick(self):
        command = ['C', '2', '0.6']
        drink_maker = DrinkMaker()
        drink = drink_maker.process_command(command)
        expected = 'Coffee-2 sugar-stick'
        self.assertEqual(expected, drink.order)

    def test_extra_hot_chocolate_with_sugar_and_stick(self):
        command = ['Hh', '', '0.5']
        drink_maker = DrinkMaker()
        drink = drink_maker.process_command(command)
        expected = 'Chocolate-Extra hot'
        self.assertEqual(expected, drink.order)


if __name__ == '__main__':
    unittest.main()
