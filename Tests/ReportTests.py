import unittest

from Classes.Drink import Drink
from Classes.Drinks.Chocolate import Chocolate
from Classes.Drinks.Coffee import Coffee
from Classes.Report import Report

# How many of each drink was sold and the total amount of money earned so far


class ReportTest(unittest.TestCase):
    def test_drink_is_added_to_report(self):
        report = Report()
        coffee = Coffee()
        report.add(coffee)

        expected = 1
        actual = report.count

        self.assertEqual(expected, actual)

    def test_amount_of_money_earned(self):
        report = Report()
        chocolate = Chocolate()
        coffee = Coffee()
        report.add(chocolate)
        report.add(coffee)

        expected = 1.1
        actual = report.get_money_earned()

        self.assertEqual(expected, actual)


if __name__ == '__main__':
    unittest.main()

# 05/04 Continue with the implementation. It doesn't have to be perfect.
