import sys

from Classes.DrinkMaker import DrinkMaker
from Classes.DrinkMaker import Drink


def report():
    pass


def drink_maker():
    values = input("Enter command separated by commas: ")
    command_list = values.split(",")
    command_tuple = tuple(command_list)
    print('Tuple : ', command_tuple)

    coffee_machine = DrinkMaker()
    result = coffee_machine.process_command(command_tuple)

    if isinstance(result, Drink):
        print(result.order)
    else:
        print(result)
    menu()


def menu():
    print("--------Welcome to drink maker--------")
    choice = input("""
                      A: Drink Maker
                      B: Report
                      Q: Logout

                      Please enter your choice: """)

    if choice == "A" or choice == "a":
        drink_maker()
    elif choice == "B" or choice == "b":
        report()
    elif choice == "Q" or choice == "q":
        sys.exit
    else:
        print("You must only select either A or B")
        print("Please try again")
        menu()


if __name__ == "__main__":
    menu()
# 3/31 Main is not working.. need to kind a way to make the process command generic...?
# When the money is not enough a message is send so make_drink does not always return a drink
# Not sure what it should do...

# Mini habits, don't give up just yet :)

# 4/20
# Im not 100% sure about price implementation. Should drink implement this? I think it should but I don't
# know how would that affect the implementation of the decorator pattern

# Next step: Implement Console Menu so you can choose send a command or see the generated reports
# 4/23 Menu implemented :)

# Next step: https://simcap.github.io/coffeemachine/cm-fourth-iteration.html
# Implement tests for the report. Where and when should the data be stored? also.. I need to upload the repository
